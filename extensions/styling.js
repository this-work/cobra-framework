/**
 * Cobra-Framework Styling Extension
 *
 * Version 1.0.0
 * Author Tobias Wöstmann
 *
 */

import Vue from 'vue';

/**
 * Vue styling extension
 * Extend the vue context with extracted SCSS vars from the framework and his overwrites
 */
export default function () {
    Vue.prototype.$styling = <%= JSON.stringify(options) %> ;
}

/**
 * Styling export vars
 * Independent variables from extracted SCSS for partial loading via import into JS files
 */
export const styling = <%= JSON.stringify(options) %> ;
<% for (const [key, value] of Object.entries(options)) { %>export const <%= key %> = <%= JSON.stringify(value) %>;
<% } %>
