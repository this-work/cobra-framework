/**
 * Cobra-Framework Merge Strategy Extension
 *
 * Version 1.0.0
 * Author Tobias Wöstmann
 *
 */

import Vue from 'vue';

/**
 * Overwrite the default Vue merge strategy in the computed hook with the blockClasses property.
 * Does not overwrite the blockClasses property but extends it
 */
export default function() {

    const evaluateFn = fnContext => maybeFn => typeof maybeFn === 'function' ? maybeFn.call(fnContext) : maybeFn;
    const identity = input => input;

    Vue.config.optionMergeStrategies.computed = function(parentVal, childVal) {

        if (!parentVal) return childVal;
        if (!childVal) return parentVal;

        const parentClasses = [].concat(parentVal['blockClasses']);
        const childClasses = [].concat(childVal['blockClasses']);

        return {
            ...parentVal,
            ...childVal,
            blockClasses() {
                return []
                    .concat(
                        ...parentClasses.map(evaluateFn(this)),
                        ...childClasses.map(evaluateFn(this))
                    )
                    .filter(identity);
            }
        };

    };
}
