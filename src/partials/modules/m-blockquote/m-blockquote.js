/**
 * m-blockquote
 */

import { common, spacing, theme, background } from '../../../plugins/mixins';

export default {

    name: 'm-blockquote',

    mixins: [
        ...common,
        spacing,
        theme,
        background
    ],

    props: {
        layout: {
            type: String,
            default: 'l-8-4'
        },
        blockquote: Object
    },

    computed: {
        blockClasses() {
            return [
            ];
        },
        verifiedLayout() {
            if (this.blockquote && this.blockquote.alignment) {
                switch (this.blockquote.alignment) {
                    case 'center':
                        return 'l-2-8-2';
                    case 'right':
                        return 'l-4-8';
                    default:
                        return this.layout;
                }
            }
        },
        getSlotName() {
            if (this.verifiedLayout === 'l-2-8-2' || this.verifiedLayout === 'l-4-8') {
                return 'column-2';
            } else {
                return 'column-1';
            }
        }
    },

    methods: {
    }
};
