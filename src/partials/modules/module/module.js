/**
 * module
 */
import { common, background } from '../../../plugins/mixins';

export default {

    name: 'module',

    mixins: [
        ...common,
        background
    ],

    props: {
        tag: {
            type: String,
            default: 'div'
        }
    }

};
