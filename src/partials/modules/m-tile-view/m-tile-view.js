/**
 * m-tile-view
 */

import { common, spacing, theme, background } from '../../../plugins/mixins';

export default {

    name: 'm-tile-view',

    mixins: [
        ...common,
        spacing,
        theme,
        background
    ],

    props: {
        type: {
            type: String,
            default: 'slider'
        },
        tiles: Array,
        heading: Object,
        text: Object
    },

    data() {
        return {
            sliderOptions: {
                autoHeight: false,
                loop: false,
                pagination: false,
                navigation: true,
                slidesPerView: 'auto',
                freeMode: true,
                mousewheel: {
                    forceToAxis: true
                },
                controlPosition: 'top',
                spaceBetween: this.$styling.gridConfig.default['column-gutter']
            }
        };
    },

    computed: {
        blockClasses() {
            return {
                [`${this.$options.name}--type-${this.type}`]: true
            };
        }
    },

    methods: {
    }
};
