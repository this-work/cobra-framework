/**
 * m-marquee
 */

import { common, spacing, theme, background } from '../../../plugins/mixins';

export default {

    name: 'm-marquee',

    mixins: [
        ...common,
        spacing,
        theme,
        background
    ],

    props: {
        layoutArea: {
            type: String,
            default: 'viewport'
        },
        heading: Object,
        rows: Array
    },

    computed: {
        blockClasses() {
            return [
            ];
        }
    },

    methods: {
    }
};
