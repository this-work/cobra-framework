/**
 * m-multicolumn
 */

import { common, spacing, theme, background } from '../../../plugins/mixins';

export default {

    name: 'm-multicolumn',

    mixins: [
        ...common,
        spacing,
        theme,
        background
    ],

    props: {
        heading: Object,
        columns: {
            type: Number,
            default: 2
        },
        verticalAlign: String,
        content: Array
    },

    computed: {
        blockClasses() {
            const className = this.$options.name;

            return {
                [`${className}--vertical-align-${this.verticalAlign}`]: this.verticalAlign,
                [`${className}--columns-${this.columns}`]: true
            };
        }
    },

    methods: {
        getColumnAlignment(column) {
            if (!column.alignment) return null;
            return `${this.$options.name}__item--align-${column.alignment}`;
        }
    }
};
