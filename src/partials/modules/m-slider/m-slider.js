/**
 * m-slider
 */

import { common, spacing, theme, background } from '../../../plugins/mixins';

export default {

    name: 'm-slider',

    mixins: [
        ...common,
        spacing,
        theme,
        background
    ],

    props: {
        options: Object,
        slides: Array,
        heading: Object,
        text: Object,
        layoutArea: String,
        slideArea: {
            type: String,
            default: 'viewport'
        }
    },

    computed: {
        blockClasses() {
            return [
            ];
        },
        slideAreaClasses() {
            return {
                [`${this.$options.name}__slide-area`]: true,
                [`${this.$options.name}__slide-area--top-control-space`]:  this.options.controlPosition === 'top' && ((!this.heading || (!this.heading.overline && !this.heading.headline && !this.heading.subline)) && !this.text)
            };
        }
    },

    methods: {
    }
};
