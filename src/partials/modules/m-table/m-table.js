/**
 * m-table
 */

import { common, spacing, theme, background } from '../../../plugins/mixins';

export default {

    name: 'm-table',

    mixins: [
        ...common,
        spacing,
        theme,
        background
    ],

    props: {
        heading: Object,
        rows: Array
    },

    computed: {
        blockClasses() {
            return [
            ];
        }
    },

    methods: {
    }
};
