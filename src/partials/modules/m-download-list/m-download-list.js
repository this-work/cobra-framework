/**
 * m-download-list
 */

import { common, spacing, theme, background } from '../../../plugins/mixins';

export default {

    name: 'm-download-list',

    mixins: [
        ...common,
        spacing,
        theme,
        background
    ],

    props: {
        heading: Object,
        items: Array
    },

    computed: {
        blockClasses() {
            return [
            ];
        }
    },

    methods: {
    }
};
