/**
 * m-divider
 */

import { common, spacing, theme, background } from '@this/cobra-framework/src/plugins/mixins';

export default {

    name: 'm-divider',

    mixins: [
        ...common,
        spacing,
        theme,
        background
    ],

    props: {
        headline: String,
        layoutArea: {
            type: String,
            default: 'content'
        },
    },

    computed: {
        blockClasses() {
            return [
            ];
        },
    },

    methods: {
    }
};
