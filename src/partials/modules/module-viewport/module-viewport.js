/**
 * module-viewport
 */
import { common } from '../../../plugins/mixins';

export default {

    name: 'module-viewport',

    mixins: [
        ...common
    ],

    props: {
        tag: {
            type: String,
            default: 'div'
        }
    }
};
