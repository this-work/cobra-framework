/**
 * m-tabs
 */

import { common, spacing, theme, background } from '../../../plugins/mixins';

export default {

    name: 'm-tabs',

    mixins: [
        ...common,
        spacing,
        theme,
        background
    ],

    props: {
        heading: Object,
        tabs: Array,
        defaultTabIndex: {
            type: Number,
            default: 0
        }
    },

    computed: {
        blockClasses() {
            return [
            ];
        }
    },

    methods: {
    }
};
