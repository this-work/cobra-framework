0/**
 * m-parallax
 */

import { common, spacing, theme, background } from '@this/cobra-framework/src/plugins/mixins';

export default {

    name: 'm-parallax',

    mixins: [
        ...common,
        spacing,
        theme,
        background
    ],

    directives: {
        slide: {
            inserted: (element, bind, vnode) => {
                vnode.elm.dispatchEvent(new CustomEvent('inserted'));
            }
        }
    },

    props: {
        sections: Array,
        scaleValue: { type: Number, default: 1.5 }
    },

    data() {
        return {
            observer: null
        };
    },

    beforeMount() {
        this.createCustomVariables();

        if (!process.client && !process.browser) return;

        if (CSS
            && CSS.supports
            && CSS.supports('position', 'sticky')
            && 'IntersectionObserver' in window
            && 'IntersectionObserverEntry' in window
            && 'intersectionRatio' in window.IntersectionObserverEntry.prototype) {

            this.observer = new IntersectionObserver(this.intersectionObservingOperator, {
                threshold: this.createObserverThreshold(300)
            });
        }
    },

    mounted() {
        if (!process.client && !process.browser) return;
        if (this.observer) return;

        this.$el.classList.add(this.modifier('static'));
        setTimeout(() => {
            this.$el.querySelectorAll('.' + this.element('media')).forEach((element, index) => {
                element.style.top = index * 100 + 'vh';
            });
        }, 500);
    },

    beforeDestroy() {
        if (this.observer) {
            this.observer.disconnect();
        }
    },

    methods: {

        observeSlide(event) {
            if (this.observer) {
                setTimeout(() => {
                    this.observer.observe(event.target);
                }, 200);
            }
        },

        getLayout(section) {
            switch (section.textColumns) {
                case 3:
                    return section.alignment === 'left' ? 'l-3-1-8' : 'l-8-1-3';
                case 4:
                    return section.alignment === 'left' ? 'l-4-1-7' : 'l-7-1-4';
                case 5:
                    return section.alignment === 'left' ? 'l-5-1-6' : 'l-6-1-5';
                case 6:
                    return section.alignment === 'left' ? 'l-6-1-5' : 'l-5-1-6';
                case 7:
                    return section.alignment === 'left' ? 'l-7-1-4' : 'l-4-1-7';
                case 8:
                    return section.alignment === 'left' ? 'l-8-1-3' : 'l-3-1-8';
            }
        },

        getSlotName(section) {

            if (section.alignment === 'right') {

                if (this.$i18n && this.$i18n.localeProperties.dir && this.$i18n.localeProperties.dir === 'rtl') {
                    return 'column-1';
                }

                return 'column-3';
            }

            if (this.$i18n && this.$i18n.localeProperties.dir && this.$i18n.localeProperties.dir === 'rtl') {
                return 'column-3';
            }

            return 'column-1';

        },


        createObserverThreshold(steps) {
            return Array(steps + 1)
                .fill(0)
                .map((_, index) => index / steps || 0);
        },

        intersectionObservingOperator(entries) {
            entries.forEach(entry => {
                this.animateMedia(entry, parseInt(entry.target.getAttribute('data-parallax-slide')));
            });
        },

        animateMedia(section, index) {

            const media = this.$refs.media
                ? this.$refs.media[index].$el
                : this.$el.querySelector('.' + this.element('media') + '[data-parallax-slide=\'' + index + '\'] > *');
            const visibilityAreaPercent = section.intersectionRect.height / window.innerHeight;
            let opacity = 1;
            let scale;

            if (section.boundingClientRect.y > 0) {
                // Entering
                if (index == 0) {
                    scale = this.customVariables.minScale;
                } else {
                    scale = this.customVariables.minScale + visibilityAreaPercent * this.customVariables.scaleFactor;
                    opacity = visibilityAreaPercent >= 0.95 ? 1 : visibilityAreaPercent;
                }
            } else {
                // Leaving
                if (index == 0) {
                    scale = this.customVariables.normalScale - visibilityAreaPercent * this.customVariables.scaleFactor;
                } else {
                    scale = this.customVariables.maxScale - visibilityAreaPercent * this.customVariables.scaleFactor;
                }
            }

            media.style.opacity = (Math.round(opacity * 1000) / 1000).toString();
            media.style.transform = 'translate3d(0,0,0) scale(' + Math.round(scale * 1000) / 1000 + ')';
        },

        createCustomVariables() {
            this.customVariables = {
                minScale: 1,
                normalScale: (this.scaleValue - 1) / 2 + 1,
                maxScale: this.scaleValue,
                scaleFactor: (this.scaleValue - 1) / 2
            };
        }
    }

};
