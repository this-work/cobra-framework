/**
 * m-drag-slider
 */

import { common, spacing, theme, background } from '@this/cobra-framework/src/plugins/mixins';

export default {

    name: 'm-drag-slider',

    dragging: {
        shift: 0,
        moduleWidth: 0,
        savedActiveSlidePosition: 0,
    },

    mixins: [
        ...common,
        spacing,
        theme,
        background
    ],

    props: {
        draggable: {
            type: Boolean,
            default: true
        },
        layoutArea: {
            type: String,
            default: 'page'
        },
        initialPosition: {
            type: Number,
            default: 0
        },
        transition: {
            type: String,
            default: 'none'
        },
        heading: Object,
        images: Array
    },

    data() {
        return {
            activeSlide: this.initialPosition
        };
    },

    computed: {
        blockClasses() {
            return {
                [`${this.block}--draggable`]: this.draggable,
                [`${this.block}--transition-${this.transition}`]: this.transition,
            };
        }
    },

    mounted() {
        setTimeout(() => {
            if (this.draggable) {
                this.registerDraggingEvents();
            }
        },500);
    },

    methods: {

        registerDraggingEvents() {

            const $element = this.$refs.images;

            $element.addEventListener('touchstart', (event) => {
                this.handleDraggingEvent(event.touches[0].clientX, 'touchmove', ['touchend']);
            });

            $element.addEventListener('mousedown', (event) => {
                this.handleDraggingEvent(event.clientX, 'mousemove', ['mouseup', 'mouseleave']);
            });

        },

        handleDraggingEvent(clientX, moveEventName, endEventNames) {


            const $element = this.$refs.images;
            const eventListeners = [];

            this.$options.dragging.shift = clientX;
            this.$options.dragging.moduleWidth = $element.offsetWidth;
            this.$options.dragging.savedActiveSlidePosition = 100 / this.images.length * this.activeSlide;

            document.addEventListener(moveEventName, this.changeSlideByMovement);

            endEventNames.forEach(endEventName => {
                $element.addEventListener(endEventName, eventListeners[endEventName] = () => {
                    document.removeEventListener(moveEventName, this.changeSlideByMovement);
                    $element.removeEventListener(endEventName, eventListeners[endEventName], false);
                }, false);
            });

            if (moveEventName === 'mousedown') {
                $element.ondragstart = () => {
                    return false;
                };
            }

        },

        changeSlideByMovement(event) {

            const percentage = ((event.pageX - this.$options.dragging.shift) / this.$options.dragging.moduleWidth * 100 ) * -1;

            this.setActiveSlide(Math.min(Math.max(Math.round(this.images.length * ((this.$options.dragging.savedActiveSlidePosition - percentage) / 100)), 0), (this.images.length - 1)));

        },

        setActiveSlide(value) {
            this.activeSlide = value;
        }
    }
};
