/**
 * m-button
 */

import { common, spacing, theme, background, alignment } from '../../../plugins/mixins';

export default {

    name: 'm-button',

    mixins: [
        ...common,
        spacing,
        theme,
        background,
        alignment
    ],

    props: {
        button: Object
    },

    computed: {
        blockClasses() {
            return [
            ];
        }
    },

    methods: {
    }
};
