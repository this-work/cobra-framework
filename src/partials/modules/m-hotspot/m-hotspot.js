/**
 * m-hotspot
 */

import { common, spacing, theme, background } from '../../../plugins/mixins';

export default {

    name: 'm-hotspot',

    mixins: [
        ...common,
        spacing,
        theme,
        background
    ],

    props: {
        layout: {
            type: String,
            default: 'l-12'
        },
        layoutArea: {
            type: String,
            default: 'content'
        },
        heading: Object,
        text: Object,
        hotspots: Array,
        image: Object
    },

    data() {
        return {
            modalContent: {}
        };
    },

    computed: {
        blockClasses() {
            return [
            ];
        },
        getSlotName() {
            if (this.layout === 'l-1-10-1') {
                return 'column-2';
            } else if (this.layout === 'l-2-8-2') {
                return 'column-2';
            } else {
                return 'column-1';
            }
        }
    },

    methods: {
        getPositionAttribute(hotspot) {
            const xPosition = hotspot.position.x * 100;
            const yPosition = hotspot.position.y * 100;
            return `top: ${yPosition}%; left: ${xPosition}%;`;
        },
        openModal(hotspot) {
            this.modalContent = hotspot;
            this.$refs.modal.open();
        }
    }
};
