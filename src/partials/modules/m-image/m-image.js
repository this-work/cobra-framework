/**
 * m-image
 */

import { common, spacing, theme, background } from '../../../plugins/mixins';

export default {

    name: 'm-image',

    mixins: [
        ...common,
        spacing,
        theme,
        background
    ],

    props: {
        layout: {
            type: String,
            default: 'l-12'
        },
        layoutArea: {
            type: String,
            default: 'content'
        },
        heading: Object,
        image: Object
    },

    computed: {
        blockClasses() {
            return [
            ];
        },
        getSlotName() {
            if (this.layout === 'l-1-10-1') {
                return 'column-2';
            } else if (this.layout === 'l-2-8-2') {
                return 'column-2';
            } else {
                return 'column-1';
            }
        }
    },

    methods: {
    }
};
