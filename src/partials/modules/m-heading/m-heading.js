/**
 * m-heading
 */

import { common, spacing, theme, background } from '../../../plugins/mixins';

export default {

    name: 'm-heading',

    mixins: [
        ...common,
        spacing,
        theme,
        background
    ],

    props: {
        layout: {
            type: String,
            default: 'l-12'
        },
        heading: Object
    },

    computed: {
        blockClasses() {
            return [
            ];
        },
        getSlotName() {
            if (this.layout === 'l-2-8-2' ||
                this.layout === 'l-4-8' ||
                this.layout === 'l-1-10-1') {
                return 'column-2';
            } else if (this.layout === 'l-5-1-6') {
                return 'column-3';
            } else {
                return 'column-1';
            }
        }
    },

    methods: {
    }
};
