/**
 * module-content
 */
import { common } from '../../../plugins/mixins';

export default {

    name: 'module-content',

    mixins: [
        ...common
    ],

    props: {
        tag: {
            type: String,
            default: 'div'
        }
    }
};
