/**
 * m-checklist
 */

import { common, spacing, theme, background } from '../../../plugins/mixins';

export default {

    name: 'm-checklist',

    mixins: [
        ...common,
        spacing,
        theme,
        background
    ],

    props: {
        heading: Object,
        items: Array,
        downloadButton: Object
    },

    computed: {
        blockClasses() {
            return [
            ];
        }
    },

    methods: {
    }
};
