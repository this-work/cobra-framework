/**
 * m-anchor-navigation
 */

import { common, theme, background } from '@this/cobra-framework/src/plugins/mixins';

export default {

    name: 'm-anchor-navigation',

    mixins: [
        ...common,
        theme,
        background
    ],

    props: {
        items: Array
    }
};
