/**
 * m-download
 */

import { common, spacing, theme, background, alignment } from '../../../plugins/mixins';

export default {

    name: 'm-download',

    inheritAttrs: false,

    mixins: [
        ...common,
        spacing,
        theme,
        background,
        alignment
    ],

    props: {
        buttonLabel: String,
        description: String,
        url: String
    },

    computed: {
        blockClasses() {
            return [
            ];
        }
    },

    methods: {
    }
};
