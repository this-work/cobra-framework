/**
 * module-page
 */
import { common } from '../../../plugins/mixins';

export default {

    name: 'module-page',

    mixins: [
        ...common
    ],

    props: {
        tag: {
            type: String,
            default: 'div'
        }
    }
};
