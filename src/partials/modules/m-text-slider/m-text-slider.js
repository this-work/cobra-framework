/**
 * m-text-slider
 */

import { common } from '@this/cobra-framework/src/plugins/mixins';

export default {

    name: 'm-text-slider',

    inheritAttrs: false,

    mixins: [
        ...common
    ],

    props: {
        options: Object,
        slides: Array
    }
};
