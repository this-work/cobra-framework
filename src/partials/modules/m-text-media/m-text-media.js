/**
 * m-text-media
 */

import { common, spacing, theme, background } from '../../../plugins/mixins';

export default {

    name: 'm-text-media',

    mixins: [
        ...common,
        spacing,
        theme,
        background
    ],

    props: {
        textColumns: {
            type: Number,
            default: 6
        },
        order: {
            type: String,
            default: 'media-first',
            validate: value => [ 'media-first', 'text-first' ].includes(value)
        },
        mobileOrder: {
            type: String,
            default: 'media-first',
            validate: value => [ 'media-first', 'text-first' ].includes(value)
        },
        verticalAlign: {
            type: String,
            default: 'center',
            validate: value => [ 'top', 'center', 'bottom' ].includes(value)
        },
        mobileMediaWidth: {
            type: String,
            default: 'content',
            validate: value => [ 'content', 'viewport' ].includes(value)
        },
        breakoutImage: {
            type: String,
            default: null,
            validate: value => [ 'top', 'bottom', null ].includes(value)
        }
    },

    computed: {

        blockClasses() {

            const className = this.$options.name;

            return {
                [`${className}--order-${this.order}`]: true,
                [`${className}--mobile-order-${this.mobileOrder}`]: true,
                [`${className}--mobile-media-width-${this.mobileMediaWidth}`]: true,
                [`${className}--vertical-align-${this.verticalAlign}`]: true,
                [`${className}--breakout-image-${this.breakoutImage}`]: this.breakoutImage
            };
        },
        getTextColumnClass() {

            const layout = this.layout();

            if (this.order === 'text-first') {
                return layout + '__column-1';
            } else {
                return layout + '__column-3';
            }
        },
        getMediaColumnClass() {

            const layout = this.layout();

            if (this.order === 'media-first') {
                return layout + '__column-1';
            } else {
                return layout + '__column-3';
            }
        },

    },

    methods: {

        layout() {

            switch (this.textColumns) {
                case 3:
                    return this.order === 'text-first' ? 'l-3-1-8' : 'l-8-1-3';
                case 4:
                    return this.order === 'text-first' ? 'l-4-1-7' : 'l-7-1-4';
                case 5:
                    return this.order === 'text-first' ? 'l-5-1-6' : 'l-6-1-5';
                case 6:
                    return this.order === 'text-first' ? 'l-6-1-5' : 'l-5-1-6';
                case 7:
                    return this.order === 'text-first' ? 'l-7-1-4' : 'l-4-1-7';
                case 8:
                    return this.order === 'text-first' ? 'l-8-1-3' : 'l-3-1-8';
            }

        }

    }
};
