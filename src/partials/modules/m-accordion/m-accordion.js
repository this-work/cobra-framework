/**
 * m-accordion
 */

import { common, spacing, theme, background } from '../../../plugins/mixins';

export default {

    name: 'm-accordion',

    mixins: [
        ...common,
        spacing,
        theme,
        background
    ],

    props: {
        heading: Object,
        panels: Array,
        openOnlyOnePanel: { type: Boolean, default: false },
        layoutArea: String
    },

    computed: {
        blockClasses() {
            return [
            ];
        }
    },

    methods: {
    }
};
