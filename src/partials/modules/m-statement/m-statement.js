/**
 * m-statement
 */

import { common, spacing, theme, background, alignment } from '../../../plugins/mixins';

export default {

    name: 'm-statement',

    mixins: [
        ...common,
        spacing,
        theme,
        background,
        alignment
    ],

    props: {
        heading: Object,
        quote: String,
        author: String,
        image: Object
    },

    computed: {
        blockClasses() {
            return [
            ];
        }
    },

    methods: {
    }
};
