/**
 * m-ab-slider
 */

import { common, spacing, theme, background } from '../../../plugins/mixins';

export default {

    name: 'm-ab-slider',

    mixins: [
        ...common,
        spacing,
        theme,
        background
    ],

    props: {
        layoutArea: {
            type: String,
            default: 'content'
        },
        heading: Object,
        abslider: Object
    },

    computed: {
        blockClasses() {
            return [
            ];
        }
    },

    methods: {
    }
};
