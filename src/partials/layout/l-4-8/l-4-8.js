/**
 * l-8-4
 */

import { common } from '../../../plugins/mixins';

export default {

    name: 'l-4-8',

    mixins: [
        ...common
    ],

    computed: {
        blockClasses() {
            return [
            ];
        }
    },

    methods: {

    }
};
