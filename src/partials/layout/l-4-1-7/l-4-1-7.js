/**
 * l-4-1-7
 */

import { common } from '@this/cobra-framework/src/plugins/mixins';

export default {

    name: 'l-4-1-7',

    mixins: [
        ...common
    ],

    computed: {
        blockClasses() {
            return [
            ];
        }
    },

    methods: {

    }
};
