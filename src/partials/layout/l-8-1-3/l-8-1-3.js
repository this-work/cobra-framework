/**
 * l-8-1-3
 */

import { common } from '@this/cobra-framework/src/plugins/mixins';

export default {

    name: 'l-8-1-3',

    mixins: [
        ...common
    ],

    computed: {
        blockClasses() {
            return [
            ];
        }
    },

    methods: {

    }
};
