/**
 * layout
 */

import { common } from '../../../plugins/mixins';

export default {

    name: 'layout',

    mixins: [
        ...common
    ],

    props: {
        layout: {
            type: String,
            default: 'l-0'
        }
    },

    computed: {
    },

    methods: {

    }
};
