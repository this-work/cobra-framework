/**
 * l-6-6
 */

import { common } from '@this/cobra-framework/src/plugins/mixins';

export default {

    name: 'l-6-6',

    mixins: [
        ...common
    ]

};
