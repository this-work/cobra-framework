/**
 * l-3-1-8
 */

import { common } from '@this/cobra-framework/src/plugins/mixins';

export default {

    name: 'l-3-1-8',

    mixins: [
        ...common
    ],

    computed: {
        blockClasses() {
            return [
            ];
        }
    },

    methods: {

    }
};
