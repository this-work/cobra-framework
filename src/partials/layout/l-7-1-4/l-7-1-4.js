/**
 * l-7-1-4
 */

import { common } from '@this/cobra-framework/src/plugins/mixins';

export default {

    name: 'l-7-1-4',

    mixins: [
        ...common
    ],

    computed: {
        blockClasses() {
            return [
            ];
        }
    },

    methods: {

    }
};
