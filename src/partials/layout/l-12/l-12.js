/**
 * l-fullwidth
 */

import { common } from '../../../plugins/mixins';

export default {

    name: 'l-12',

    mixins: [
        ...common
    ],

    props: {
    },

    computed: {
        blockClasses() {
            return [
            ];
        }
    },

    methods: {

    }
};
