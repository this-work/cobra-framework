/**
 * c-tooltip
 */

import { common } from '../../../plugins/mixins';

export default {

    name: 'c-tooltip',

    mixins: [
        ...common
    ],

    props: {
        tag: {
            type: String,
            default: 'div'
        },
        trigger: {
            type: String,
            default: 'hover'
        },
        position: {
            type: String,
            default: 'top-center'
        },
        icon: String,
        text: String,
        content: String
    },

    data() {
        return {
            visible: false
        };
    },

    computed: {
        blockClasses() {
            return {
                [`${this.$options.name}--position-${this.position}`]: true
            };
        }
    },

    methods: {

    }
};
