/**
 * c-list
 */

import { common } from '../../../plugins/mixins';

export default {

    name: 'c-list',

    mixins: [
        ...common
    ],

    props: {
        tag: {
            type: String,
            default: 'ul',
            validator(value) {
                return [ 'ul', 'ol' ].includes(value);
            }
        },
        title: String,
        items: Array
    }
};
