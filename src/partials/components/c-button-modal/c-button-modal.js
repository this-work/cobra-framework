/**
 * c-button-modal
 */

import { common } from '../../../plugins/mixins';

export default {

    name: 'c-button-modal',

    inheritAttrs: false,

    mixins: [
        ...common
    ],

    props: {
        tag: {
            type: String,
            default: 'span'
        },
        fullwidth: Boolean,
        fullscreen: Boolean,
        heading: Object,
        text: String,
        nuggets: Array
    },

    computed: {
        blockClasses() {
            return [
            ];
        }
    },

    methods: {
        openModal() {
            this.$refs.modal.open();
        }
    }
};
