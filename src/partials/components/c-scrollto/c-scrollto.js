/**
 * c-scrollto
 */

import { common } from '../../../plugins/mixins';
import merge from 'deepmerge';

export default {

    name: 'c-scrollto',

    mixins: [
        ...common
    ],

    props: {
        tag: {
            type: String,
            default: 'a'
        },
        target: {
            type: String,
            default: 'body'
        },
        offset: {
            type: Number,
            default: -30
        },
        duration: {
            type: Number,
            default: 300
        },
        options: {
            type: Object,
            default: () => ({})
        }
    },

    data() {
        return {
            defaultOptions: {
                container: 'body',
                easing: [ 0.42, 0.0, 0.58, 1.0 ],
                force: true,
                cancelable: false
            }
        };
    },

    computed: {
        blockClasses() {
            return [
            ];
        },

        scrollToOptionsObject() {
            const customOptions = merge(this.defaultOptions, {
                el: this.target,
                duration: this.duration,
                offset: this.offset
            });
            return merge(customOptions, this.options);
        }
    },

    methods: {

    }
};
