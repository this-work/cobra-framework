/**
 * c-input-checkbox
 */

import { common } from '../../../plugins/mixins';

export default {

    name: 'c-input-checkbox',

    inheritAttrs: false,

    mixins: [
        ...common
    ],

    model: {
        prop: 'checked',
        event: 'change'
    },

    props: {
        tag: {
            type: String,
            default: 'div'
        },
        disabled: Boolean,
        readonly: Boolean,
        tabindex: {
            type: Number,
            default: 0
        },
        checked: {
            type: Boolean,
            default: false
        },
        valid: {
            type: Boolean,
            default: true
        },
        size: String,
        label: String
    },

    data() {
        return {
            value: this.checked
        };
    },

    computed: {
        blockClasses() {
            return {
                [`${this.$options.name}--size-${this.size}`]: this.size,
                [`${this.$options.name}--size-disabled`]: this.disabled
            };
        },
        listeners() {
            /* eslint-disable-next-line no-unused-vars */
            const { change, ...listeners } = this.$listeners;
            return listeners;
        }
    },

    watch: {
        checked(bool) {
            this.value = bool;
        }
    },

    methods: {
        toggle() {
            if (this.disabled || this.readonly) return;
            this.value = !this.value;
        }
    }
};
