/**
 * c-button-audio
 */

import { common } from '../../../plugins/mixins';

export default {

    name: 'c-button-audio',

    inheritAttrs: false,

    mixins: [
        ...common
    ],

    props: {
        tag: {
            type: String,
            default: 'c-button'
        },
        label: String,
        audio: String,
        loop: Boolean
    },

    data() {
        return {
            activeAudio: false,
            audioElement: null
        };
    },

    computed: {
        blockClasses() {
            return {
                [this.modifier('playing')]: this.activeAudio
            };
        },

        audioIsActive() {
            return this.activeAudio;
        }
    },

    mounted() {
        if (this.audio) {
            this.audioElement = new Audio(this.audio);
            this.audioElement.loop = this.loop;
            this.addEventListeners();
        }
    },

    beforeDestroy() {
        if (this.activeAudio) {
            this.fadeOutAudio(this.audioElement);
        }
    },

    methods: {
        addEventListeners() {
            if (!process.client && !process.browser) return;
            this.audioElement.onended = this.onAudioEnded;
        },

        onAudioEnded() {
            this.activeAudio = false;
        },

        fadeInAudio(audioElement) {
            audioElement.volume = 0.0;
            audioElement.play();
            this.activeAudio = true;

            const fadeInAudio = setInterval(() => {
                if (Number(audioElement.volume).toFixed(1) !== '1.0') {
                    audioElement.volume += 0.1;
                }
                if (Number(audioElement.volume).toFixed(1) === '1.0') {
                    clearInterval(fadeInAudio);
                }
            }, 20);

            setTimeout(() => {
                clearInterval(fadeInAudio);
            }, 200);
        },

        fadeOutAudio(audioElement) {
            const fadeOutAudio = setInterval(() => {
                if (Number(audioElement.volume).toFixed(1) !== '0.0') {
                    audioElement.volume -= 0.1;
                }
                if (Number(audioElement.volume).toFixed(1) === '0.0') {
                    clearInterval(fadeOutAudio);
                }
            }, 20);

            setTimeout(() => {
                clearInterval(fadeOutAudio);
                audioElement.pause();
                this.activeAudio = false;
            }, 200);

        },

        toggleAudio() {
            if (!this.activeAudio) {
                this.fadeInAudio(this.audioElement);
            } else {
                this.fadeOutAudio(this.audioElement);
            }
        }
    }
};
