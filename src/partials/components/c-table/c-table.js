/**
 * c-table
 */

import { common } from '../../../plugins/mixins';

export default {

    name: 'c-table',

    mixins: [
        ...common
    ],

    props: {
        rows: Array
    },

    computed: {
        blockClasses() {
            return [
            ];
        }
    },

    methods: {

    }
};
