/**
 * c-accordion
 */

import { common } from '../../../plugins/mixins';
import merge from 'deepmerge';

export default {

    name: 'c-accordion',

    mixins: [
        ...common
    ],

    props: {
        panels: {
            type: Array,
            default: () => []
        },
        dropdown: {
            type: Boolean,
            default: true
        },
        accordionDefaults: {
            type: Object,
            default: () => {}
        }
    },

    data() {
        return {
            panelStatus: []
        };
    },

    computed: {
        blockClasses() {
            return [
            ];
        }
    },

    beforeMount() {
        this.panels.forEach((panel, index) => {
            if (panel.initiallyOpened) {
                this.toggleOpen(index);
            }
        });
    },

    methods: {
        merge: merge,

        isOpen(index) {
            return this.panelStatus[index] ? this.panelStatus[index].open : false;
        },

        toggleOpen(index) {

            let panelStatusOpen = false;

            if (this.panelStatus[index]) {
                panelStatusOpen = this.panelStatus[index].open;
            }

            if (this.dropdown) {
                for (let _index = 0; _index < this.panels.length; _index++) {
                    this.$set(this.panelStatus, _index, { open: false });
                }
            }

            this.$set(this.panelStatus, index, { open: !panelStatusOpen });

        }
    }
};
