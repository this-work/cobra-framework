/**
 * c-image
 */

import { common, responsiveAspectRatios } from '../../../plugins/mixins';

if (process.client || process.browser) {
    const objectFitImages = require('object-fit-images');
    require('picturefill');
    objectFitImages();
}

/**
 *
 */
export default {

    name: 'c-image',

    mixins: [
        ...common,
        responsiveAspectRatios
    ],

    inject: {
        providedDefaultLazyLoad: { default: true }
    },

    props: {
        tag: { type: String, default: 'figure' },
        inline: { type: Boolean, default: false },
        src: { type: String },
        srcset: { type: Object, default: () => ({}) },
        srcAlternates: { type: Array, default: () => ([]) },
        sizes: { type: String },
        objectFit: { type: String, default: null },
        objectPosition: { type: String, default: null },
        skeleton: { type: Boolean, default: false },
        overlay: { type: Boolean, default: false },
        overlayMobileHidden: { type: Boolean, default: false },
        caption: { type: String },
        captionPosition: { type: String, default: 'bottom' },
        information: { type: String, default: null },
        informationPosition: { type: String, default: 'bottom-left' },
        lightboxImage: { type: Object },
        alt: { type: String },
        lazyload: { type: Boolean, default() {
            return this.providedDefaultLazyLoad;
        } }
    },

    data() {
        return {
            showSkeleton: true,
            tooltipPosition: 'top-right'
        };
    },

    mounted() {

        setTimeout(() => {
            if (this.$el.querySelector('img').complete) {
                this.loadEvent();
            }
        }, 20);

    },

    computed: {

        blockClasses() {
            return {
                [`${this.$options.name}--type-inline`]: this.inline
            };
        },

        getCanvasClasses() {
            return {
                [`${this.$options.name}__canvas-overlay`]: this.overlay,
                [`${this.$options.name}__canvas-overlay--hide-mobile`]: this.overlayMobileHidden,
                [`${this.$options.name}__canvas-skeleton`]: this.skeleton,
                [`${this.$options.name}__canvas-skeleton--hide`]: this.skeleton && !this.showSkeleton
            };
        },

        getCanvasInlineStyling() {
            return {
                height: this.aspectRatio ? 'auto' : null,
                paddingTop: this.aspectRatio
            };
        },

        getImageBindings() {

            const lazyloadPrefix = this.lazyload ? 'data-' : '';

            return {
                class: this.getImageClasses,
                style: this.getImageInlineStyling,
                title: this.alt,
                alt: this.alt,
                [`${lazyloadPrefix}sizes`]: this.sizes,
                [`${lazyloadPrefix}src`]: this.src,
                [`${lazyloadPrefix}srcset`]: this.getSourceset()
            };
        },

        getAlternateSources() {

            return JSON.parse(JSON.stringify(this.srcAlternates)).sort(this.sortMediaquerysDownwards).map(({ mediaquery, srcsets = {} }) => {

                const lazyloadPrefix = this.lazyload ? 'data-' : '';

                return Object.entries(srcsets).map(([ fileType, srcSet ]) => {

                    const set = typeof srcSet !== 'string' ? this.getSourceset(srcSet) : srcSet;

                    if (fileType === 'jpg') { fileType = 'jpeg'; }

                    const type = `image/${fileType}`;

                    return {
                        type: type,
                        media: mediaquery,
                        [`${lazyloadPrefix}srcset`]: set,
                        sizes: this.sizes
                    };

                });

            }).reduce((acc, cur) => ([ ...acc, ...cur ]), []);

        },

        getImageClasses() {
            return {
                [`${this.$options.name}__element`]: true,
                [`${this.$options.name}__element--absolute`]: this.aspectRatio,
                'lazyload': this.lazyload
            };
        },

        getImageInlineStyling() {

            const objectFit = this.objectFit ? this.objectFit : (this.aspectRatio ? 'cover' : null);

            const polyfillString = [
                objectFit && `object-fit: ${objectFit}`,
                this.objectPosition && `object-position: ${this.objectPosition}`
            ].filter(x => x).join(';') || null;

            return {
                'object-fit': objectFit,
                'object-position': this.objectPosition,
                'font-family': polyfillString,
                'opacity': process.browser ? null : 0
            };
        },

        captionTag() {
            return (this.tag === 'figure' ? 'figcaption' : 'div');
        },

        getCaptionClasses() {
            return [
                `${this.$options.name}__caption--valign-${this.captionPosition}`
            ];
        },

        getInformationClasses() {
            return [
                `${this.$options.name}__information--align-${this.informationPosition}`
            ];
        },

        getTooltipPosition() {
            switch (this.informationPosition) {
                case 'top-left':
                    return 'bottom-right';
                case 'top-right':
                    return 'bottom-left';
                case 'bottom-left':
                    return 'top-right';
                case 'bottom-right':
                    return 'top-left';
            }
        }

    },

    methods: {

        sortMediaquerysDownwards(firstEntry, secondEntry) {

            if (!firstEntry.mediaquery) {
                return 1;
            }
            if (!secondEntry.mediaquery) {
                return -1;
            }

            const firstEntryWidth = parseInt(firstEntry.mediaquery.replace(/\D/g, ''));
            const secondEntryWidth = parseInt(secondEntry.mediaquery.replace(/\D/g, ''));

            if (firstEntryWidth > secondEntryWidth) {
                return -1;
            }
            if (firstEntryWidth < secondEntryWidth) {
                return 1;
            }

            return 0;

        },

        getSourceset(srcset = this.srcset) {
            return Object.entries(srcset)
                .map(([ descriptor, url ]) => `${url} ${descriptor}`)
                .join(', ');
        },

        loadEvent() {
            this.showSkeleton = false;
            this.$emit('load', true);
        },

        openModal() {
            this.$refs.modal.open();
        }

    }
};
