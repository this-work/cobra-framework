/**
 * c-slider
 */

import { common } from '../../../plugins/mixins';

import { Swiper, SwiperSlide } from 'vue-awesome-swiper';
import 'swiper/css/swiper.css';

import merge from 'deepmerge';

export default {

    name: 'c-slider',

    components: {
        Swiper,
        SwiperSlide
    },

    mixins: [
        ...common
    ],

    props: {
        tag: {
            type: String,
            default: 'div'
        },
        options: {
            type: Object,
            default: () => ({})
        },
        slides: Array,
        slideDefaults: {
            type: Object,
            default: () => ({})
        }
    },

    data() {
        return {
            defaultOptions: {
                'autoHeight': true,
                'slidesPerView': 1,
                'loop': false,
                'fadeEffect': {
                    crossFade: true
                },
                'autoplay': false,
                'threshold': 10,
                'controlPosition': 'bottom',
                'navigation': {
                    nextEl: `.${this.$options.name}__next-slide`,
                    prevEl: `.${this.$options.name}__previous-slide`,
                    disabledClass: `disabled`
                },
                'pagination': {
                    el: `.${this.$options.name}__pagination`,
                    type: 'fraction',
                    clickable: true
                }
            },
            swiper: false,
            slideDescription: false
        };
    },

    beforeDestroy() {

        document.removeEventListener('lazyloaded', this.updateSwiper);

    },

    computed: {

        blockClasses() {
            const swiperOptions = this.mergeSwiperOptions();
            return {
                [`${this.$options.name}--controls-${swiperOptions.controlPosition}`]: (swiperOptions.navigation !== false || swiperOptions.pagination !== false)
            };
        },

        swiperOptions() {
            return this.mergeSwiperOptions();
        }
    },

    methods: {

        sliderReady() {
            if (!this.swiper) {
                this.swiper = this.$refs.swiper.$swiper;
                this.updateSwiper();
            }
            this.updateSlideDescription();
            document.addEventListener('lazyloaded', this.updateSwiper);

        },

        updateSlideDescription() {
            if (!this.swiper) {
                return;
            }
            if (this.slides && this.slides.length > 0) {
                this.slideDescription = this.slides[this.swiper.realIndex].description;
            }
        },

        loadedSlide() {

            const options = this.mergeSwiperOptions();

            if (!this.swiper) {
                this.swiper = this.$refs.swiper.$swiper;
            }

            this.updateSwiper();

            if (options.autoHeight) {

                let updateIntervalPassed = 0;
                const updateIntervalTime = 1000;
                const updateInterval = 100;

                const updateIntervalFunc = setInterval(() => {

                    updateIntervalPassed += updateInterval;

                    if (this.swiper) {
                        this.swiper.updateAutoHeight();
                    }

                    if (updateIntervalPassed > updateIntervalTime) {
                        clearInterval(updateIntervalFunc);
                    }
                }, updateInterval);

                this.$nextTick(() => {
                    this.$forceUpdate();
                });

            }

        },

        updateSwiper() {

            const options = this.mergeSwiperOptions();

            if (!this.swiper) {
                return;
            }

            this.swiper.update();

            if (options.autoHeight) {
                this.swiper.updateAutoHeight();
            }

            if (options.navigation) {
                this.swiper.navigation.destroy();
                this.swiper.navigation.init();
                this.swiper.navigation.update();
            }

        },

        mergeSwiperOptions() {

            if (Object.keys(this.options).length !== 0 && this.options.hasOwnProperty('navigation')) {
                if (this.options.navigation === true) {
                    this.options.navigation = this.defaultOptions.navigation;
                }
            }

            if (Object.keys(this.options).length !== 0 && this.options.hasOwnProperty('pagination')) {
                if (this.options.pagination === true) {
                    this.options.pagination = this.defaultOptions.pagination;
                }
            } else {
                this.options.pagination = false;
            }

            return merge(this.defaultOptions, this.options);
        },

        moduleProps(props) {
            return merge(props, this.slideDefaults);
        }
    }
};
