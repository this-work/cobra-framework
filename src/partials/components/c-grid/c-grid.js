/**
 * c-grid
 */

import { common } from '../../../plugins/mixins';

if (process.client || process.browser) {
    const { cssGridPolyfill } = require('@this/cssgrid-polyfill');
    setTimeout(() => cssGridPolyfill.init(), 0);
}

export default {

    name: 'c-grid',

    mixins: [
        ...common
    ],

    props: {
        tag: { type: String, default: 'div' }
    }
};
