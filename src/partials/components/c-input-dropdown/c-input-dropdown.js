/**
 * c-input-dropdown
 */

import { common } from '../../../plugins/mixins';

import Multiselect from 'vue-multiselect';
import 'vue-multiselect/dist/vue-multiselect.min.css';

export default {

    name: 'c-input-dropdown',

    inheritAttrs: false,

    components: {
        Multiselect
    },

    mixins: [
        ...common
    ],

    props: {
        tag: {
            type: String,
            default: 'div'
        },
        appearance: {
            type: String,
            default: 'underlined'
        },
        options: {
            type: Array,
            default: () => ([])
        },
        optionsLabel: {
            type: String
        },
        inputValue: {
            type: String
        },
        placeholder: {
            type: String,
            default: ''
        },
        showLabels: {
            type: Boolean,
            default: false
        },
        allowEmpty: {
            type: Boolean,
            default: false
        },
        searchable: {
            type: Boolean,
            default: false
        },
        autoWidth: {
            type: Boolean,
            default: false
        },
        closeOnSelect: {
            type: Boolean,
            default: true
        },
        label: {
            type: String
        },
        multiple: {
            type: Boolean,
            default: false
        },
        name: {
            type: String
        },
        preselectFirst: {
            type: Boolean,
            default: false
        },
        showNoResults: {
            type: Boolean,
            default: false
        },
        valid: {
            type: Boolean,
            default: true
        }
    },

    data() {
        return {
            value: this.$attrs.value
        };
    },

    computed: {
        blockClasses() {
            return {
                [this.modifier(this.appearance)]: this.appearance,
                'error': !this.valid,
                [this.modifier('multiple')]: this.multiple,
                [this.modifier('auto-width')]: this.autoWidth,
            };
        },
        getDropdownValue() {
            if (!this.value) {
                return null;
            }
            return this.optionsLabel ? (this.inputValue ? this.value[this.inputValue] : this.value[this.optionsLabel]) : this.value;
        }
    }
};
