/**
 * c-text
 */

import { common } from '../../../plugins/mixins';

export default {

    name: 'c-text',

    mixins: [
        ...common
    ],

    props: {
        tag: { type: String, default: 'div' },
        text: String,
        textColumns: { type: [ String, Number ], default: null }
    },

    computed: {
        blockClasses() {
            return {
                [`${this.$options.name}--columns`]: this.textColumns,
                [`${this.$options.name}--columns-${this.textColumns}`]: this.textColumns
            };
        }
    }

};
