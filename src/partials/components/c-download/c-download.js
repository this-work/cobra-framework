/**
 * c-download
 */

import { common } from '../../../plugins/mixins';

export default {

    name: 'c-download',

    inheritAttrs: false,

    mixins: [
        ...common
    ],

    props: {
        tag: {
            type: String,
            default: 'c-button'
        },
        buttonLabel: {
            type: [ String, Boolean ],
            default: 'Download'
        },
        description: {
            type: String,
            default: 'Download'
        },
        icon: {
            type: String,
            default: 'getApp'
        },
        appearance: String,
        url: String
    }
};
