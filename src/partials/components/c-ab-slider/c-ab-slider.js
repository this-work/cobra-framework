/**
 * c-ab-slider
 */

import { common } from '../../../plugins/mixins';

export default {

    name: 'c-ab-slider',

    mixins: [
        ...common
    ],

    props: {
        tag: {
            type: String,
            default: 'div'
        },
        before: Object,
        after: Object,
        initalPosition: {
            type: Number,
            default: 50
        },
        padding: {
            type: Object,
            default() {
                return {
                    left: 22,
                    right: 22
                };
            }
        }
    },

    data() {
        return {
            width: null,
            height: null,
            pageX: null,
            posX: null,
            isDragging: false,
            allowNextFrame: true,
            unwatch: null
        };
    },

    computed: {
        dimensions() {
            return {
                width: `${this.width}px`,
                height: 'auto'
            };
        }
    },

    methods: {
        onResize() {
            this.width = this.$el.clientWidth;
            this.height = this.$el.clientHeight;
            this.setInitialPosX(this.padding.left + this.padding.right);
        },
        onMouseDown() {
            this.isDragging = true;
        },
        onMouseUp(event) {
            event.preventDefault();
            this.isDragging = false;
        },
        onMouseMove(event, isDragging = this.isDragging) {
            if (isDragging && this.allowNextFrame) {
                this.allowNextFrame = false;
                this.pageX = event.pageX || event.targetTouches[0].pageX || event.originalEvent.targetTouches[0].pageX;
                window.requestAnimationFrame(this.updatePos);
            }
        },
        updatePos() {
            let posX = this.pageX - this.$el.getBoundingClientRect().left;
            if (posX < this.padding.left) {
                posX = this.padding.left;
            } else if (posX > this.width - this.padding.right) {
                posX = this.width - this.padding.right;
            }
            this.posX = posX;
            this.allowNextFrame = true;
        },
        setInitialPosX(padding) {
            if (padding >= this.width) {
                return;
            }

            const minimalPosition = this.padding.left;
            const maximumPosition = this.width - this.padding.right;

            let initalPosition = (this.width + this.padding.left - this.padding.right) / (100 / this.initalPosition);

            if (initalPosition < minimalPosition) {
                initalPosition = minimalPosition;
            }

            if (initalPosition > maximumPosition) {
                initalPosition = maximumPosition;
            }

            this.posX = initalPosition;

        }
    },

    created() {
        if (process.client || process.browser) {
            window.addEventListener('mouseup', this.onMouseUp);
            window.addEventListener('resize', this.onResize);
        }
    },
    mounted() {
        this.onResize();
        this.unwatch = this.$watch(
            () => this.padding.left + this.padding.right,
            newValue => this.setInitialPosX(newValue)
        );
    },
    beforeDestroy() {
        this.unwatch();
        if (process.client || process.browser) {
            window.removeEventListener('mouseup', this.onMouseUp);
            window.removeEventListener('resize', this.onResize);
        }
    }
};
