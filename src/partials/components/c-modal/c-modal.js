/**
 * c-modal
 */

import { common, theme } from '../../../plugins/mixins';

export default {

    name: 'c-modal',

    inheritAttrs: false,

    mixins: [
        ...common,
        theme
    ],

    data() {
        return {
            show: false
        };
    },

    props: {
        fullwidth: {
            type: Boolean,
            default: false
        },
        fullscreen: {
            type: Boolean,
            default: false
        },
        closeAppearance: { type: String, default: 'icon' },
        additionalClasses: [ String, Array ]
    },

    computed: {
        blockClasses() {
            return {
                [this.modifier('fullwidth')]: this.fullwidth,
                [this.modifier('fullscreen')]: this.fullscreen,
                [this.additionalClasses]: this.additionalClasses
            };
        }
    },

    beforeDestroy() {
        if (this.show) {
            this.$scrollBlocker.disable();
        }
    },

    methods: {

        open() {
            //  misplacedElements should be set via scrollBlocker.nuxt.config.js
            //  For compatibility reasons they are still included here.
            //  They will be removed here at a later date.
            this.$scrollBlocker.enable({
                misplacedElements: () => [
                    {
                        element: document.body,
                        property: 'right'
                    },
                    {
                        element: document.querySelector(`.m-scrollto-bar`),
                        property: 'padding-right'
                    },
                    {
                        element: document.querySelector(`.f-header__actions`),
                        property: 'padding-right'
                    }
                ]
            });

            this.show = true;
        },

        close() {

            this.$scrollBlocker.disable();
            this.show = false;

        }

    }
};
