/**
 * c-input-text
 */

import { common } from '../../../plugins/mixins';

export default {

    name: 'c-input-text',

    inheritAttrs: false,

    mixins: [
        ...common
    ],

    model: {
        prop: 'value',
        event: 'change'
    },

    props: {
        tag: {
            type: String,
            default: 'div'
        },
        appearance: {
            type: String,
            default: 'underlined'
        },
        label: {
            type: String
        },
        valid: {
            type: Boolean,
            default: true
        },
        prefix: {
            type: String
        },
        suffix: {
            type: String
        }
    },

    data() {
        return {
            value: this.$attrs.value
        };
    },

    computed: {
        blockClasses() {
            return {
                [this.modifier('prefix')]: this.prefix || this.slotHasContent('prefix'),
                [this.modifier('suffix')]: this.suffix || this.slotHasContent('suffix'),
                [this.modifier(this.appearance)]: this.appearance
            };
        }
    },

    mounted() {
        setTimeout(this.setInitalValues, 0);
    },

    methods: {

        setInitalValues() {
            const input = this.$el.querySelector('input');
            if (input) {
                input.setAttribute('value', input.value);
            }
        },

        checkValue: event => {
            if (event.target.type === 'number' && event.key.length === 1 && event.key.match(/[a-zA-Z]/i)) {
                event.preventDefault();
                return false;
            }
        },

        setValue: event => {
            event.target.setAttribute('value', event.target.value);
        }

    }
};
