/**
 * c-video
 */

import { common, responsiveAspectRatios } from '../../../plugins/mixins';

if (process.client || process.browser) {

    require('objectFitPolyfill');

}

export default {

    name: 'c-video',

    mixins: [
        ...common,
        responsiveAspectRatios
    ],

    props: {
        tag: { type: String, default: 'figure' },
        sources: { type: Object, default: () => {
                return {};
            } },
        loop: { type: Boolean, default: false },
        autoplay: { type: Boolean, default: false },
        playsinline: { type: Boolean, default: true },
        muted: { type: Boolean, default: false },
        pictureInPicture: { type: Boolean, default: false },
        controls: { type: Boolean, default: false },
        controlsDownload: { type: Boolean, default: false },
        controlsFullscreen: { type: Boolean, default: true },
        poster: { type: String, default: '' },
        posterPlayText: { type: String, default: '' },
        objectFit: { type: String, default: null },
        objectPosition: { type: String, default: null },
        overlay: { type: Boolean, default: false },
        overlayMobileHidden: { type: Boolean, default: false },
        skeleton: { type: Boolean, default: true },
        caption: { type: String },
        track: { type: Object },
        captionPosition: { type: String, default: 'bottom' },
        embedded: { type: Object, default: () => {
                return {};
            } }
    },

    data() {
        return {
            showSkeleton: this.skeleton,
            showPoster: this.poster.length > 0,
            overlayHidden: false
        };
    },

    computed: {

        getCanvasClasses() {
            return {
                [`${this.$options.name}__canvas`]: true,
                [`${this.$options.name}__canvas-overlay`]: this.overlay,
                [`${this.$options.name}__canvas-overlay--hide`]: this.overlay && this.overlayHidden,
                [`${this.$options.name}__canvas-overlay--hide-mobile`]: this.overlay && this.overlayMobileHidden,
                [`${this.$options.name}__canvas-skeleton`]: this.skeleton,
                [`${this.$options.name}__canvas-skeleton--hide`]: this.skeleton && !this.showSkeleton
            };
        },

        getEmbeddedInlineStyling() {
            if (!this.embedded || !this.embedded.ratio) {
                return false;
            }
            return {
                paddingTop: this.embedded.ratio + '%'
            };
        },

        getCanvasInlineStyling() {
            return {
                height: this.aspectRatio ? 'auto' : null,
                paddingTop: this.aspectRatio
            };
        },

        getVideoClasses() {
            return {
                [`${this.$options.name}__element`]: true,
                [`${this.$options.name}__element--absolute`]: this.aspectRatio
            };
        },

        getVideoInlineStyling() {
            return {
                'object-fit': this.objectFit ? this.objectFit : (this.aspectRatio ? 'cover' : null),
                'object-position': this.objectPosition
            };
        },

        getPosterClasses() {
            return {
                [`${this.$options.name}__poster`]: true,
                [`${this.$options.name}__poster--hide`]: !this.showPoster
            };
        },

        getPosterInlineStyling() {
            return {
                'background-image': 'url("' + this.poster + '")',
                'object-fit': this.objectFit ? this.objectFit : (this.aspectRatio ? 'cover' : null),
                'object-position': this.objectPosition
            };
        },

        getPlaysinline() {
            return this.autoplay || this.playsinline;
        },

        isMuted() {
            return !!(this.muted || this.autoplay);
        },

        getControlsList() {

            let controlsList

            if (!this.controls) {
                return null;
            }

            if (!this.controlsDownload) {
                controlsList += ' nodownload'
            }

            if (!this.controlsFullscreen) {
                controlsList += ' nofullscreen'
            }

            return controlsList;
        },

        captionTag() {
            return (this.tag === 'figure' ? 'figcaption' : 'div');
        },

        getCaptionClasses() {
            return [
                `${this.$options.name}__caption`,
                `${this.$options.name}__caption--valign-${this.captionPosition}`
            ];
        },

        getTrackLabel() {
            return (this.$i18n.localeProperties && this.$i18n.localeProperties.name) ? this.$i18n.localeProperties.name : this.$i18n.locale;
        },

        isEmbedded() {
            return Object.keys(this.embedded).length !== 0;
        },

        getEmbeddedUrl() {

            if (this.embedded.providerName === 'YouTube') {
                const splittetEmbeddedUrl = this.embedded.url.split('?v=');
                let options = [];

                if (!this.controls) options.push('controls=0');
                if (this.autoplay) options.push('autoplay=1')
                if (this.loop) {
                    options.push('loop=1');
                    options.push(`playlist=${splittetEmbeddedUrl[1]}`)
                }
                if (this.isMuted) options.push('mute=1');
                options = (!splittetEmbeddedUrl[1].includes('?') ? '?' : '&') + options.join('&');

                return 'https://www.youtube-nocookie.com/embed/' + splittetEmbeddedUrl[1] + options;
            } else if (this.embedded.providerName === 'Vimeo') {
                const splittetEmbeddedUrl = this.embedded.url.split('com/');
                const path = splittetEmbeddedUrl[1].split('/')[0];
                const hParam = splittetEmbeddedUrl[1].split('/')[1];
                return 'https://player.vimeo.com/video/' + path + '?h=' + hParam;
            }

            return this.embedded.url;

        }

    },

    mounted() {
        if (this && this.$refs && this.$refs.videoElement) {

            window.objectFitPolyfill(this.$refs.videoElement);

            setTimeout(() => {
                if (this && this.$refs && this.$refs.videoElement && this.$refs.videoElement.readyState === 4) {
                    this.loadEvent();
                }
            }, 5);

        }

    },

    methods: {

        startVideoFromPoster() {
            this.$emit('startedFromPoster', true);
            this.$el.querySelector('video').play();
            this.showPoster = false;
            this.overlayHidden = true;
        },

        loadEvent() {
            this.$emit('load', true);
            this.showSkeleton = false;
        },

        videoEndEvent() {
            this.$emit('ended', true);
            if (this.poster.length > 0) {
                this.showPoster = true;
                this.overlayHidden = false;
            }
        }
    }
};
