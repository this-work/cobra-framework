/**
 * c-link
 */

import { common } from '../../../plugins/mixins';

export default {

    name: 'c-link',

    mixins: [
        ...common
    ],

    props: {
        tag: String,
        url: String,
        externalUrl: String,
        target: String
    },

    computed: {

        props() {

            if (this.linkTag === 'nuxt-link') {
                return {
                    is: 'nuxt-link',
                    to: this.href
                };
            }

            if (this.linkTag === 'a') {
                return {
                    is: this.linkTag,
                    href: this.href,
                    target: this.target
                };
            }

            return {
                'is': this.linkTag,
                'to': this.url,
                'href': this.externalUrl
            };

        },

        href() {

            if (this.externalUrl) {
                return this.externalUrl;
            }

            if (this.url) {
                return this.url;
            }

            return undefined;

        },

        linkTag() {

            if (this.tag) {
                return this.tag;
            }

            if (this.externalUrl ||
                (this.url && this.target)) {
                return 'a';
            }

            if (this.url) {
                return 'nuxt-link';
            }

            return 'span';

        }
    }
};
