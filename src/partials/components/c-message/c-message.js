/**
 * c-message
 */

import { common } from '../../../plugins/mixins';

export default {

    name: 'c-message',

    mixins: [
        ...common
    ],

    props: {
        message: String,
        error: Boolean
    },

    computed: {
        blockClasses() {
            const className = this.$options.name;
            return {
                [`${className}--error`]: this.error
            };
        }
    },

    methods: {

    }
};
