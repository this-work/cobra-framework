/**
 * c-marquee
 */

import { common } from '../../../plugins/mixins';
import exportedVars from './c-marquee.scss';

let MarqueeText;

if (process.client || process.browser) {
    MarqueeText = require('vue-marquee-text-component');
}

export default {

    name: 'c-marquee',

    components: {
        MarqueeText
    },

    mixins: [
        ...common
    ],

    props: {
        repeat: {
            type: Number,
            default: 3
        },
        durationFactor: {
            type: Number,
            default: 4
        },
        direction: {
            type: String,
            default: 'left'
        },
        paused: {
            type: Boolean,
            default: false
        },
        content: {
            type: Array
        }
    },

    data() {
        return {
            contentRepeat: this.repeat,
            forceUpdateKey: 1,
            resizeFunction: null,
            duration: this.durationFactor,
            originWidth: parseFloat(exportedVars.originWidth)
        };
    },

    mounted() {
        requestAnimationFrame(() => {
            this.updateMarqueeSettings();
            this.resizeFunction = this.debounce(this.updateMarqueeSettings);
            window.addEventListener('resize', this.resizeFunction);
        });
    },

    beforeDestroy() {
        window.removeEventListener('resize', this.resizeFunction);
    },

    updated() {
        this.updateMarqueeSettings();
    },

    methods: {
        forceUpdateMarquee() {
            this.forceUpdateKey++;
        },

        updateMarqueeSettings() {
            if (!this.$refs.marquee) return;

            const item = this.$refs.marquee.querySelector('.marquee-text-content > *');
            if (!item) return;

            const itemWidth = parseInt(getComputedStyle(item).width);
            const marqueeWidth = parseInt(getComputedStyle(this.$refs.marquee).width);
            if (itemWidth == 0) return;

            const additionalContentAgainstEmptyGaps = 1;
            const contentRepeat = Math.ceil(marqueeWidth / itemWidth) + additionalContentAgainstEmptyGaps;
            const duration = itemWidth / this.originWidth * this.durationFactor;

            if (contentRepeat !== this.contentRepeat || duration !== this.duration) {
                this.contentRepeat = contentRepeat;
                this.duration = duration;
                this.forceUpdateMarquee();
            }
        },

        debounce(func) {
            let timer;

            return function(event) {
                if (timer) clearTimeout(timer);
                timer = setTimeout(func, 200, event);
            };
        }
    }
};
