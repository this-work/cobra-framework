/**
 * c-tabs
 */

import { common } from '../../../plugins/mixins';

export default {

    name: 'c-tabs',

    mixins: [
        ...common
    ],

    provide: {
        providedDefaultLazyLoad: false
    },

    props: {
        tabs: Array,
        defaultTabIndex: {
            type: Number,
            default: 0
        }
    },

    data() {
        return {
            activeTabIndex: this.defaultTabIndex,
            stickyTabHead: false
        };
    },

    mounted() {
        this.checkForStickyHead();
    },

    computed: {

        blockClasses() {
            return {
                [this.modifier('sticky')]: this.stickyTabHead
            };
        },

        activeContent() {
            return this.tabs[this.activeTabIndex].content;
        },

        hasTitles() {
            return this.tabs.some(tab => tab.title);
        },

        hasThumbnails() {
            return this.tabs.some(tab => tab.thumbnail);
        },

        titles() {
            return this.tabs.map(({ title }, index) => ({ title, index }));
        },

        thumbnails() {
            return this.tabs.map(({ thumbnail }, index) => ({ thumbnail, index }));
        }

    },

    methods: {
        showTab(value) {
            if (this.stickyTabHead) {
                this.$scrollTo(
                    this.$el,
                    300
                );
            }
            this.checkForStickyHead();
            this.activeTabIndex = value;
            this.$emit('changedTabIndex', value);
        },
        checkForStickyHead() {

            const stickyTabHeadInterval = setInterval(() => {
                this.stickyTabHead = (parseInt(this.$refs.content.offsetHeight) + 50 ) >
                    parseInt(window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight);
            }, 150);

            setTimeout(() => {
                clearInterval(stickyTabHeadInterval);
            }, 1501);

        }
    }
};
