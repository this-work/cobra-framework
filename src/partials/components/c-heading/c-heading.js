/**
 * c-heading
 */

import { common, alignment } from '../../../plugins/mixins';

const typoClass = typo => typo ? `is-typo-${typo}` : '';
const validValues = values => value => values.includes(value);
const VALID_TAGS = [ 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'span', 'div' ];

export default {

    name: 'c-heading',

    mixins: [
        ...common,
        alignment
    ],

    props: {
        tag: {
            type: String,
            default: 'div'
        },
        overline: String,
        overlineTag: {
            type: String,
            default: 'span',
            validator: validValues(VALID_TAGS)
        },
        overlineType: {
            type: String,
            default: 'overline'
        },
        overlineAttrs: Object,

        headline: String,
        headlineTag: {
            type: String,
            default: 'h2',
            validator: validValues(VALID_TAGS)
        },
        headlineType: {
            type: String,
            default: 'h2'
        },
        headlineAttrs: Object,

        subline: String,
        sublineTag: {
            type: String,
            default: 'span',
            validator: validValues(VALID_TAGS)
        },
        sublineType: String,
        sublineAttrs: Object
    },

    computed: {

        blockClasses() {
            return [
                this.modifier(this.headlineType)
            ];
        },

        cSublineType() {
            if (!this.sublineType) {
                if (this.headlineType === 'h0' || this.headlineType === 'h1' || this.headlineType === 'h2' || this.headlineType === 'h3') {
                    return 'subline--big';
                } else {
                    return 'subline';
                }
            } else {
                return this.sublineType;
            }
        },

        overlineClass() {
            return [
                typoClass(this.overlineType),
                this.overlineAttrs ? this.overlineAttrs.class : ''
            ];
        },
        headlineClass() {
            return [
                typoClass(this.headlineType),
                this.headlineAttrs ? this.headlineAttrs.class : ''
            ];
        },
        sublineClass() {
            return [
                typoClass(this.cSublineType),
                this.sublineAttrs ? this.sublineAttrs.class : ''
            ];
        }
    }
};
