/* eslint-disable no-unused-vars */
/**
 * c-input-range
 */

import { common } from '../../../plugins/mixins';

export default {

    name: 'c-input-range',

    model: {
        prop: 'value',
        event: 'input'
    },

    mixins: [...common],

    props: {
        tag: { type: String, default: 'div' },
        min: { type: Number, default: 0 },
        max: { type: Number, default: 100 },
        step: { type: Number, default: 1 },
        value: { type: Number, default: 50 },
        size: { type: String, default: 'big' },
        filled: { type: Boolean, default: true },
        disabled: { type: Boolean }
    },

    data() {
        return {
            isDragging: false,
            _value: 0,
            hasFocus: false
        };
    },

    watch: {
        value: {
            immediate: true,
            handler: function(newValue) {
                this.computedValue = newValue;
                this.$emit('change', this.computedValue);
            }
        },
        disabled: {
            handler: function(newValue, oldValue) {
                if (newValue === oldValue) return;
                if (!newValue) return;

                this.$el.blur();
            }
        }
    },

    computed: {
        blockClasses() {
            return {
                [`${this.block}--size-${this.size}`]: this.size,
                [`${this.block}--filled`]: this.filled,
                [this.modifier('disabled')]: this.disabled,
                [this.modifier('dragging')]: this.isDragging
            };
        },

        computedValue: {
            get() {
                return this.$data._value;
            },
            set(value) {
                if (this.step > 0) {
                    value = this.snapToStep(value);
                }
                value = this.clamp(this.min, value, this.max);
                this.$data._value = value;
            }
        },

        percentValue() {
            return (this.computedValue - this.min) / (this.max - this.min);
        },

        knobStyle() {
            return { left: `${this.percentValue * 100}%` };
        },
        fillStyle() {
            return { transform: `scaleX(${this.percentValue})` };
        }
    },

    mounted() {
        this.addEventListeners();
    },

    beforeDestroy() {
        this.removeEventListeners();
    },

    methods: {
        addEventListeners() {
            if (!process.client && !process.browser) return;

            const { track, knob } = this.$refs;

            window.addEventListener('mousedown', this.onMouseDown);
            window.addEventListener('mouseup', this.onMouseUp);

            window.addEventListener('touchstart', this.onTouchStart);
            window.addEventListener('touchend', this.onTouchEnd);
            window.addEventListener('touchcancel', this.onTouchEnd);
        },

        removeEventListeners() {
            if (!process.client && !process.browser) return;

            const { track, knob } = this.$refs;

            track.removeEventListener('mousedown', this.onMouseDown);
            knob.removeEventListener('mousedown', this.onMouseDown);
            window.removeEventListener('mouseup', this.onMouseUp);

            track.removeEventListener('touchstart', this.onTouchStart);
            knob.removeEventListener('touchstart', this.onTouchStart);
            window.removeEventListener('touchend', this.onTouchEnd);
            window.removeEventListener('touchcancel', this.onTouchEnd);

            window.removeEventListener('mousemove', this.onMouseMove);
            window.removeEventListener('touchmove', this.onTouchMove);
        },

        // onResize(event) {
        //     console.log('onResize', event);
        // },

        onMouseDown(event) {
            if (this.disabled) return;

            const target = event.target || event.srcElement;
            if (!this.$el.contains(target)) {

                if (this.hasFocus) {
                    this.$el.blur();
                }

                return;
            }

            this.isDragging = true;
            this.$el.focus();

            window.addEventListener('mousemove', this.onMouseMove);
            window.addEventListener('touchmove', this.onTouchMove);
            this.calculateNewValue(event);
        },

        onMouseMove(event) {
            if (this.disabled) return;
            if (this._cancelFrame) { return; }

            this.cancelFrame = window.requestAnimationFrame(() => {
                this.calculateNewValue(event);
                this.cancelFrame = null;
            });
        },

        onMouseUp(event) {
            if (this.disabled) return;

            this.isDragging = false;
            window.removeEventListener('mousemove', this.onMouseMove);
            window.removeEventListener('touchmove', this.onTouchMove);
        },

        onTouchStart(event) {
            if (this.disabled) return;

            // event.preventDefault();
            this.onMouseDown(event);
        },

        onTouchMove(event) {
            if (this.disabled) return;

            this.onMouseMove(event);
        },

        onTouchEnd(event) {
            if (this.disabled) return;

            // event.preventDefault();
            this.onMouseUp(event);
        },

        increaseStep(times = 1) {
            if (this.disabled) return;

            this.computedValue += this.step * times;
            this.$emit('input', this.computedValue);
        },

        decreaseStep(times = 1) {
            if (this.disabled) return;

            this.computedValue -= this.step * times;
            this.$emit('input', this.computedValue);
        },

        calculateNewValue({ clientX, targetTouches, originalEvent }) {
            const pointerX = (clientX !== null && clientX !== undefined) ? clientX : (targetTouches[0].clientX !== null && targetTouches[0].clientX !== undefined) ? targetTouches[0].clientX : originalEvent.targetTouches[0].clientX;
            console.assert(typeof this.$refs.track !== 'undefined');

            if (this.$refs.track) {
                const { x, width } = this.$refs.track.getBoundingClientRect();
                const barPercent = (pointerX - x) / width;
                const valueRange = this.max - this.min;

                this.computedValue = (barPercent * valueRange) + this.min;


                this.$emit('input', this.computedValue);
            }
        },

        clamp(min, value, max) {
            const newValue = Math.min(max, Math.max(value, min));
            return +parseFloat(newValue).toFixed(2);
        },

        snapToStep(value) {
            return Math.round(value / this.step) * this.step;
        }
    }
};
