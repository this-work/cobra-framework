/**
 * c-blockquote
 */

import { common, alignment } from '../../../plugins/mixins';

export default {

    name: 'c-blockquote',

    mixins: [
        ...common,
        alignment
    ],

    props: {
        tag: {
            type: String,
            default: 'figure'
        },
        quote: String,
        author: String
    },

    computed: {
        blockClasses() {
            return [
            ];
        }
    },

    methods: {

    }
};
