/**
 * c-download-list
 */

import { common } from '../../../plugins/mixins';

export default {

    name: 'c-download-list',

    mixins: [
        ...common
    ],

    props: {
        tag: {
            type: String,
            default: 'div'
        },
        items: {
            type: Array
        }
    },

    computed: {
        blockClasses() {
            return [
            ];
        }
    },

    methods: {

    }
};
