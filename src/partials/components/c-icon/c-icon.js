import { common } from '../../../plugins/mixins';

export default {

    name: 'c-icon',

    mixins: [
        ...common
    ],

    props: {
        name: {
            type: String
        }
    },

    computed: {
        hasNoAriaLabel() {

            if (this.$attrs['aria-label']) {
                return this.$attrs['aria-label'].length > 0;
            }

            return false;

        }
    }

};
