/**
 * c-button
 */

import { common } from '../../../plugins/mixins';

export default {

    name: 'c-button',

    mixins: [
        ...common
    ],

    props: {
        tag: {
            type: String,
            default: 'button'
        },
        to: String,
        href: String,
        appearance: {
            type: String,
            default: 'primary'
        },
        size: {
            type: String,
            default: null
        },
        spinner: {
            type: Boolean,
            default: false
        },
        loading: {
            type: Boolean,
            default: false
        },
        label: String,
        icon: String,
        iconPosition: {
            type: String,
            default: 'right'
        }
    },

    computed: {
        blockClasses() {
            return {
                [`${this.block}--${this.appearance}`]: this.appearance,
                [`${this.block}--icon-${this.iconPosition}`]: this.icon,
                [`${this.block}--size-${this.size}`]: this.size,
                [this.modifier('loading')]: this.loading
            };
        },
        calculatedTag() {

            if (this.to) {
                return 'nuxt-link';
            }

            if (this.href || (this.tag === 'nuxt-link' && !this.to)) {
                return 'a';
            }

            return this.tag;

        },
        attributes() {
            return {
                ...(this.to && { to:this.to }),
                ...(this.href && !this.to && { href:this.href })
            };
        }

    },

    methods: {

    }
};
