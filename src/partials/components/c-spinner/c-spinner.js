import { common } from '../../../plugins/mixins';

export default {

    name: 'c-spinner',

    mixins: [
        ...common
    ],

    props: {
        tag: { type: String, default: 'div' },
        fullscreen: { type: Boolean, default: false },
        size: { type: String },
        visible: { type: Boolean, default: false }
    },

    computed: {
        blockClasses() {
            return [
                this.fullscreen && `${this.$options.name}--fullscreen`
            ];
        },
        hasTextSlot() {
            return !!this.$slots.default || !!this.$scopedSlots.default;
        }
    }
};
