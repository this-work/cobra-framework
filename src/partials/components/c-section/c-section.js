/**
 * Section
 */

import { common, spacing } from '../../../plugins/mixins';

export default {

    name: 'c-section',

    mixins: [
        ...common,
        spacing
    ],

    props: {
        tag: { type: String, default: 'section' },
        showDivider: { type: Boolean, default: false },
        headline: String,
        background: { type: String, default: 'none' },
        id: String
    },

    computed: {
        blockClasses() {
            return {
                [`background-color--${this.background}`]: this.background !== 'none'
            };
        },
        theme() {
          return this.background
        }
    }

};
