/**
 * c-checklist
 */

import { common } from '../../../plugins/mixins';

export default {

    name: 'c-checklist',

    mixins: [
        ...common
    ],

    props: {
        tag: {
            type: String,
            default: 'ul',
            validator(value) {
                return [ 'ul', 'ol' ].includes(value);
            }
        },
        title: String,
        items: Array
    },

    computed: {
        blockClasses() {
            return [
            ];
        }
    },

    methods: {

    }
};
