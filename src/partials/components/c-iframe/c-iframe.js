import { common } from '../../../plugins/mixins';

/**
 * c-iframe
 */

export default {

    name: 'c-iframe',

    mixins: [
        ...common
    ],

    props: {
        src: { type: String },
        width: { type: String, default: null },
        height: { type: String, default: null },
        title: { type: String },
        cookieRestriction: { type: Boolean, default: false },
        cookieRestrictionContext: { type: String, default: '' },
        cookieRestrictionText: { type: String, default: null },
        cookieRestrictionButtonText: { type: String, default: null }
    },

    data() {
        return {
            cookieRestrictionState: false
        };
    },

    mounted() {
        if (this.cookieRestriction) {
            this.cookieRestrictionState = !!localStorage.getItem('allow-' + this.cookieRestrictionContext);
        }
    },

    computed: {
        blockClasses() {
            return [
            ];
        },
        inlineStyles() {
            return {
                'width': this.width,
                'height': this.height
            };
        },
        hasNoAriaLabel() {
            return !(this.title && this.title.length > 0);
        },
        allowedToShow() {
            if (!this.cookieRestriction) {
                return true;
            }
            return this.cookieRestrictionState;
        }
    },

    methods: {
        allowIframe() {
            localStorage.setItem('allow-' + this.cookieRestrictionContext, true);
            this.cookieRestrictionState = true;
        }
    }
};
