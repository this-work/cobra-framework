import lazySizes from 'lazysizes';
import 'lazysizes/plugins/attrchange/ls.attrchange';

lazySizes.cfg.hFac = 2;

export default lazySizes;
