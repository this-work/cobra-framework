/**
 * Scroll Blocker JS
 *
 * Handles the possible scrolling of the html root.
 *
 * Version 1.0.1
 * Author Tobias Wöstmann
 *
 */

let affectedElementsStore = [];
let breakpointStore = false;
let debouncedResizeEvent;

/**
 * @function
 * @name disablePageScrolling
 *
 * @description Disable the page scrolling on the root html node and preserve the position.
 *
 * @param {Array} affectedElements - Elements, like position:fixed ones, that must be horizontal moved with
 * @param {String|Boolean} breakpoint - Breakpoint String up to which size the scrolling should be disabled
 */
export function disablePageScrolling(affectedElements = affectedElementsStore, breakpoint = breakpointStore) {

    breakpointStore = breakpoint;

    if (!breakpoint || parseInt(breakpoint) > window.innerWidth ) {

        const scrollbarWidth = window.innerWidth - document.documentElement.clientWidth;
        const html = document.querySelector('html');

        if (scrollbarWidth !== 0) {

            html.style.marginRight = `${scrollbarWidth}px`;

            affectedElements.forEach(({ element, property }) => {

                if (element) {
                    element.style[property] = `${scrollbarWidth}px`;
                }

            });

            affectedElementsStore = affectedElements;

        }

        document.body.style.top = `-${window.scrollY}px`;

        html.classList.add('has-no-scroll');

    }

    if (breakpoint) {
        window.addEventListener('resize', debouncedPageScrollCheck);
    }

}

/**
 * @function
 * @name enablePageScrolling
 *
 * @description Enable the page scrolling on the root html node and preserve the position.
 *
 */
export function enablePageScrolling() {

    if (breakpointStore) {
        window.removeEventListener('resize', debouncedPageScrollCheck);
    }

    if (!breakpointStore || parseInt(breakpointStore) > window.innerWidth ) {

        const html = document.querySelector('html');
        const scrollY = document.body.style.top;

        html.style.marginRight = null;

        affectedElementsStore.forEach(({ element, property }) => {
            if (element) {
                element.style[property] = null;
            }
        });

        html.classList.remove('has-no-scroll');

        document.body.style.top = '';

        window.scrollTo(0, parseInt(scrollY || '0') * -1);

    }

}

/**
 * @function
 * @name debouncedPageScrollCheck
 *
 * @description Debounced check if the page scroll should be enabled or disabled.
 * Should be only used when breakpointStore is not false.
 *
 * @param {String|Boolean} breakpoint - Breakpoint String up to which size the scrolling should be disabled
 */
function debouncedPageScrollCheck(breakpoint = breakpointStore) {

    clearTimeout(debouncedResizeEvent);
    debouncedResizeEvent = setTimeout(() => {

        if ( window.innerWidth >= parseInt(breakpoint)) {

            enablePageScrolling();

        } else {

            disablePageScrolling();

        }

    }, 100);

}
