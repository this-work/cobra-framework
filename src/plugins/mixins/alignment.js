export default {
    props: {
        alignment: {
            type: String,
            default: 'left'
        }
    },

    computed: {
        blockClasses() {
            return [
                this.alignment && `text-align-${this.alignment}`
            ];
        }
    }
};
