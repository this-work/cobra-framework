export default {
    props: {
        background: { type: Boolean, default: true },
        backgroundColor: { type: String, default: null },
        backgroundImage: { type: String, default: null },
        backgroundPosition: { type: String, default: 'page' },
        backgroundDecoration: { type: Boolean, default: false },
        backgroundDecorationImage: { type: Boolean, default: false },
    }
};
