export default {
    props: {
        theme: { type: String, default: 'light' }
    },

    computed: {
        blockClasses() {
            return {
                [`theme-${this.theme}`]: this.theme
            };
        }
    }

};
