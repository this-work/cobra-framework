export const spacingClass = (property, direction, value) => value && `spacing-${property}-${direction}-${value}`;

export const spacingProps = {
    spacingMarginTop: { type: String, default: 'none' },
    spacingPaddingTop: { type: String, default: 'medium' },
    spacingMarginBottom: { type: String, default: 'none' },
    spacingPaddingBottom: { type: String, default: 'medium' }
};

export default {
    props: {
        ...spacingProps
    },

    computed: {

        blockClasses() {
            return [
                spacingClass('margin', 'top', this.spacingMarginTop),
                spacingClass('margin', 'bottom', this.spacingMarginBottom),
                spacingClass('padding', 'top', this.spacingPaddingTop),
                spacingClass('padding', 'bottom', this.spacingPaddingBottom)
            ];
        }
    }

};
