
export default {

    props: {
        aspectRatios: { type: Object, default: () => ({}) }
    },

    data() {
        return {
            aspectRatio: null
        };
    },

    mounted() {

        const initialValues = Object.entries(this.aspectRatios)
            .sort((a, b) => a[0] - b[0])
            .map(([ bp, r ]) => ([ bp, r.split(':').reduceRight((a, b) => (a / b) * 100) ]))
            .reduce((bestMatch, [bp], index, ratios) => {

                const mql = matchMedia(`(min-width: ${bp}px)`);

                const mqlAddEventListener = typeof mql.addEventListener == 'function'
                    ? mql.addEventListener.bind(mql)
                    : (eventName, handler) => mql.addListener(handler);

                const mqlRemoveEventListener = typeof mql.removeEventListener == 'function'
                    ? mql.removeEventListener.bind(mql)
                    : (eventName, handler) => mql.removeListener(handler);

                const mqlHandler = () => this.setAspectRatio(index, mql, ratios);

                mqlAddEventListener('change', mqlHandler);

                this.$once('hook:destroyed', () => mqlRemoveEventListener('change', mqlHandler));

                return mql.matches ? { index, mql, ratios } : bestMatch;
            }, null);

        if (initialValues) {
            this.setAspectRatio(initialValues.index, initialValues.mql, initialValues.ratios);
        }

    },

    methods: {

        setAspectRatio(index, mql, sortedRatios) {

            const idx = mql.matches ? index : index - 1;

            if (!mql.matches && sortedRatios[idx]) {
                const previousMediaQuery = matchMedia(`(min-width: ${sortedRatios[idx][0]}px)`);
                if (!previousMediaQuery.matches) {
                    return;
                }
            }

            if (mql.matches && sortedRatios[idx + 1]) {
                const nextMediaQuery = matchMedia(`(min-width: ${sortedRatios[idx + 1][0]}px)`);
                if (nextMediaQuery.matches) {
                    return;
                }
            }

            const ratio = sortedRatios[idx] ? sortedRatios[idx][1] : null;

            if (ratio !== 'auto') {
                this.aspectRatio = ratio == null ? null : `${ratio}%`;
            } else {
                this.aspectRatio = null;
            }

        }
    }

};
