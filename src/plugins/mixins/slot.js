export default {

    methods: {
        slotHasContent(slotName) {
            return !!this.$slots[slotName] || !!this.$scopedSlots[slotName];
        }
    }

};
