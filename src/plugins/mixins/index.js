import alignment from './alignment';
import bem from './bem';
import theme from './theme';
import slot from './slot';
import spacing from './spacing';
import background from './background';
import responsiveAspectRatios from './responsive-aspect-ratios';

export const common = [ bem, slot ];

export { alignment, bem, slot, spacing, theme, background, responsiveAspectRatios };

export default common;
