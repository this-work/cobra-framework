export default {
    props: {
        additionalClasses: String
    },
    computed: {
        block() {
            return this.$options.name;
        },

        blockClasses() {
            return [
                this.block,
                this.additionalClasses
            ];
        }
    },

    methods: {
        element(...elements) {
            return []
                .concat(elements)
                .map(element => `${this.block}__${element}`);
        },

        elementModifier(element, ...modifiers) {
            return []
                .concat(modifiers)
                .map(modifier => `${this.block}__${element}--${modifier}`);
        },

        modifier(...modifiers) {
            return []
                .concat(modifiers)
                .map(modifier => `${this.block}--${modifier}`);
        }

    }

};
