/**
 * Cobra-Framework nuxt 2.13+ integration
 *
 * Version 1.0.0
 * Author Tobias Wöstmann
 *
 */

import { join } from 'path';
import _ from 'lodash';
import extractedStyling from '../helper/scss-extraction';

export default function(moduleOptions) {

    const { nuxt } = this;
    const nuxtConfig = nuxt.options;


    /**
     * Make sure the nuxt components option is enabled
     */
    if (!nuxt.options.components) {
        throw new Error('please set `components: true` inside `nuxt.config` and ensure using `nuxt >= 2.13.0`');
    }


    /**
     * Change the default vue computed merge strategy for the 'blockClass' handling.
     * Add Plugin to the parent nuxt instance
     */
    this.addPlugin({
        src: join(__dirname, '../extensions/mergeStrategy-computed.js'),
        fileName: 'cobra-framework/vue.mergestrategy-computed.js'
    });


    /**
     * Add the theme styling to the vue/nuxt context and
     * create independent export vars to import stylings in vanilla js
     * Add Plugin to the parent nuxt instance
     */
    this.addPlugin({
        src: join(__dirname, '../extensions/styling.js'),
        fileName: 'cobra-framework/vue.styling.js',
        options: extractedStyling(
            moduleOptions.theme ?
                moduleOptions.theme :
                'node_modules/@this/cobra-framework/src/theme/scss/framework.scss'
        )
    });


    /**
     * List all nuxt modules
     */
    const modules = [
        'vue-scrollto/nuxt',
        'portal-vue/nuxt',
        '@this/scroll-blocker'
    ];


    /**
     * Add all nuxt modules to the instance and
     * require them
     */
    for (const module of modules) {
        this.requireModule(module);
    }


    /**
     * List all external plugins with mode and
     * optional css path
     */
    const plugins = {
        'lazysizes': {},
        'simplebar': { mode: 'client', css: 'simplebar/dist/simplebar.css' }
    };


    /**
     * Add all third party plugin to the nuxt instance
     * Get all plugins from the external plugin index file
     */
    for (const [ name, options ] of Object.entries(plugins)) {
        const pluginBase = {
            src: join(__dirname, `../src/plugins/external/${name}.js`),
            fileName: `cobra-framework/plugins/${name}.js`
        };
        this.addPlugin({ ...pluginBase, ...options });

        if (options.css) {
            const pluginCSS = { src: options.css };
            'css' in nuxtConfig ? nuxtConfig['css'].push(pluginCSS) : nuxtConfig['css'] = [pluginCSS];
        }

    }


    /**
     * Add the base CSS to the nuxt config of the parent nuxt instance
     */
    let baseCSS = { src: '~/node_modules/@this/cobra-framework/src/theme/scss/base.scss', lang: 'scss' };

    if (moduleOptions.baseCSS) {
        baseCSS = { src: '~/' + moduleOptions.baseCSS, lang: 'scss' };
    }

    // Check if the 'css' in the parent nuxt instance exist
    // and merge or fill it
    'css' in nuxtConfig ? nuxtConfig['css'].push(baseCSS) : nuxtConfig['css'] = [baseCSS];


    /**
     * Add the framework in the styleResources configuration object
     * of the parent nuxt instance
     */
    let styleResource = '~/node_modules/@this/cobra-framework/src/theme/scss/framework.scss';

    if (moduleOptions.theme) {
        styleResource = '~/' + moduleOptions.theme;
    }

    // Check if the styleResources configuration object exist
    if (!('styleResources' in nuxtConfig)) nuxtConfig.styleResources = {};

    // Check if the 'scss' in the styleResources configuration object exist
    // and merge or fill it
    'scss' in nuxtConfig.styleResources ? nuxtConfig.styleResources['scss'].push(styleResource) : nuxtConfig.styleResources['scss'] = [styleResource];


    /**
     * Add svg handling module
     */
    this.addModule({
        src: '@nuxtjs/svg'
    }, true);


    /**
     * Add the framework and his partys to the nuxt webpack transpiler for ES5
     */
    nuxtConfig.build.transpile = _.union(nuxtConfig.build.transpile, [
        '@this/cssgrid-polyfill',
        '@this/cobra-framework',
        'swiper',
        'dom7'
    ]);


    /**
     * List all component dirs
     */
    const componentDirs = [
        'src/theme/icons',
        'src/partials/components',
        'src/partials/frame',
        'src/partials/layout',
        'src/partials/modules',
        'src/partials/transitions'
    ];


    /**
     * Add all partials/components to the parent nuxt instance
     * All folders are separately to prevent a automatic prefix
     */
    this.nuxt.hook('components:dirs', dirs => {
        for (const componentDir of componentDirs) {
            dirs.push({
                path: join(__dirname, '../' + componentDir),
                level: 10,
                extensions: ['vue']
            });
        }
    });

}
