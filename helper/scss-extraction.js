/**
 * Cobra-Framework SCSS Extraction
 *
 * Version 1.0.0
 * Author Tobias Wöstmann
 *
 */
const sassExtract = require('sass-extract');
const camelCase = require('camel-case');
const path = require('path');
const fs = require('fs');
let cachedStyles;

if (process.env.NODE_ENV === 'production' && fs.existsSync(path.join(process.cwd(), '.nuxt/cobra-framework/vue.styling.js')) ) {
    cachedStyles = require(path.join(process.cwd(), '.nuxt/cobra-framework/vue.styling.js'));
}

/**
 * @function
 * @name scss-extraction
 *
 * @description Extract all scss variables from a given scss file to a js
 * object. Filter directly variables that should be filtered out and rename it.
 *
 * @param {String} theme - Path to scss entry file
 * @param {Array} filter - Array with variable names that should be filtered out
 * @param {Object} renames - Optional renames of SCSS Variables
 * @return {Object} - SCSS Object
 */
export default function(
    theme,
    filter = [
        '$default-conversion-context',
        '$default-class-separator',
        '$color-base',
        '$color-variations',
        '$color-derivates',
        '$debug-breakpoints',
        '$debug-spacings',
        '$default-color-palettes-map',
        '$default-zindex-map',
        '$default-spacing-map',
        '$default-font-dimensions-map',
        '$default-responsive-font-types-map',
        '$default-font-config-map',
        '$default-font-weights-map',
        '$default-breakpoint-map',
        '$default-normalize-font-dimension'
    ],
    renames = {
        defaultColorMap: 'colors'
    }) {

    if (process.env.NODE_ENV === 'production' && fs.existsSync(path.join(process.cwd(), '.nuxt/cobra-framework/vue.styling.js')) ) {
        return cachedStyles.styling;
    }

    return renameScssVariables(mapScssVariables(extractScss(theme, filter)), renames);

}


/**
 * @function
 * @name extractScss
 *
 * @description Extract all scss variables from a given scss file in a js
 * object. Filter directly variables that should be filtered out.
 *
 * @param {String} file - Path to scss entry file
 * @param {Array} filter - Array with variable names that should be filtered out
 * @return {Object} - Unmapped SCSS Object
 */
function extractScss(file, filter) {

    return sassExtract.renderSync({
        file: process.cwd() + '/' + file
    }, {
        plugins: [
            {
                plugin: 'filter',
                options: {
                    except: {
                        props: filter
                    }
                }
            }
        ]
    }).vars.global;

}


/**
 * @function
 * @name mapScssVariables
 *
 * @description Map a given object with scss informations to add a
 * unit, simplify color values and go deeper if there is any map or
 * list as value.
 *
 * @param {Object} scss - Unmapped SCSS Object
 * @return {Object} - Mapped SCSS Object
 */
function mapScssVariables(scss) {

    for (const variable in scss) {
        if (scss.hasOwnProperty(variable)) {

            if (variable === '$color-palettes') {

                scss['themes'] = Object.keys(Object.values(scss[variable].value)[0].value);

            } else {

                const variableValue = scss[variable];

                if (variableValue.type === 'SassNumber' && variableValue.unit.length > 0) {

                    scss[variable] = variableValue.value + variableValue.unit;

                } else if (variableValue.type === 'SassMap' || variableValue.type === 'SassList') {

                    scss[variable] = mapScssVariables(variableValue.value);

                } else if (variableValue.type === 'SassColor') {

                    if (variableValue.value.a === 1) {

                        scss[variable] = variableValue.value.hex;

                    } else {

                        scss[variable] = 'rgba(' + variableValue.value.r + ', ' + variableValue.value.g + ', ' + variableValue.value.b + ', ' + variableValue.value.a + ')';

                    }

                } else {

                    scss[variable] = variableValue.value;

                }
            }

        }

    }

    delete scss['$color-palettes'];

    return scss;

}


/**
 * @function
 * @name renameScssVariables
 *
 * @description Change all scss variable name to camel-case and rename
 * optional complete names.
 *
 * @param {Object} scss - SCSS Object
 * @param {Object} renames - Optional renames of SCSS Variables
 * @return {Object} - Renamed SCSS Object
 */
function renameScssVariables(scss, renames) {

    const renamedScss = {};

    for (let variable in scss) {
        if (scss.hasOwnProperty(variable)) {

            const values = scss[variable];
            variable = camelCase.camelCase(variable);

            if (values !== null) {
                for (const rename in renames) {
                    if (renames.hasOwnProperty(rename)) {
                        if (variable === rename) {
                            variable = renames[rename];
                        }
                    }
                }
                renamedScss[variable] = values;
            }

        }
    }

    return renamedScss;

}
