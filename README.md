# Cobra Framework
Component framework for Nuxt 2 or Vue 2.


### Requirements
- Vue CLI or Nuxt
- Babel proposal optional chaining plugin
- [nuxtjs/style-resources](https://github.com/nuxt-community/style-resources-module) module.


### Usage in Nuxt

Install framework dependencies
``` bash
$ npm install @this/cobra-framework
```

Add the cobra-framework and the nuxt style-resources module in nuxt.config.js as build modules
``` js
buildModules: [
    '@this/cobra-framework/nuxt',
    '@nuxtjs/style-resources',
]
```

### Own Theme
To overwrite and configure the framework styles you can add your scss with the module parameter 'baseCSS' and 'theme'.

#### Option: baseCSS
The given file defines the base styles of the project. To merge it with the default base styles of the framework
its recommend to import the default base.scss in your base file.
``` scss
@import 'node_modules/@this/cobra-framework/src/theme/scss/base';
```

#### Option: theme
The given file defines the theme of the project and can overwrite all default variable values of the framework.
To merge it with the default styles of the framework its required to import the theme.scss in your theme file.
The framework works with the '!default' scss suffix that makes it necessary to import your styles before the framework.

This file will be imported in every scss file of the project. It´s recommended to add no css attributes in this file.
``` scss
$default-border-radius: 10px;
@import 'node_modules/@this/cobra-framework/src/theme/scss/framework';
```

#### Example
``` js
buildModules: [
        [ '@this/cobra-framework/nuxt', {
            baseCSS: 'assets/theme/base.scss',
            theme: 'assets/theme/theme.scss',
        } ],
        '@nuxtjs/style-resources',
]
```
